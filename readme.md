Project split into two groups.  The Client group and the Server group.

####Dependencies
install node
`npm install` must be executed from both the root and server directory to install various module dependencies.

####Server - Created with featherjs cli

fully contained in /server directory and for testing, project root is served.

from server folder
`sh run.sh` - to start server listening on 3030
 
####Client - Created with Aurelia cli
 
from root folder
`au run --watch`


