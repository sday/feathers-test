import io from 'socket.io-client';
import feathers from 'feathers-client';

export class App {
  email="";
  authenticated="false";

  constructor() {
    var parent=this;
    const socket = io('http://localhost:3030');

    this.f = feathers()
      .configure(feathers.socketio(socket))
      .configure(feathers.hooks())
      .configure(feathers.authentication({ storage: window.localStorage }));

    // This appears to be a problem.  If I am in fact already logged in, the callback is made, the token and user object is returned.
    // If the user is not logged in the catch isn't called, but an error is generated in the browser console.
    // Error: Could not find stored JWT and no authentication type was given
    // The docs indicate one should be able to call authenticate() without parameters to validate a current session.
    this.f.authenticate().then(function(result){
      console.log('Already Authenticated!', parent.f.get('token'));
      parent.email=parent.f.get('user').email;
      console.log("User:",parent.f.get('user'));
      parent.authenticated=true;
    }).catch(function(error){
      console.error('Not authenticated!', error);
      parent.authenticated=false;
    });

  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  createUser() {
    var parent = this;

    if(!this.validateEmail(this.email)) {
      console.log("INVALID EMAIL, UNABLE TO CREATE USER");
      return;
    }
    this.f.service('users').create({
      email: this.email,
      password: "test"
    })
      .then(function() {
        console.log("%s user created", parent.email);
      })
      .catch(function(error){
        console.error('Error Creating User! %s', parent.email, error);
      });
  }

  login() {
    var parent=this;
    console.log("attempting to login with: ", this.email);

    this.f.authenticate({
      type: 'local',
      email: this.email,
      password: "test"
    }).then(function(result){
      parent.authenticated=true;
      console.log('Authenticated!', parent.f.get('token'));
      console.log('User=', parent.f.get('user').email);
    }).catch(function(error){
      parent.authenticated=false;
      console.error('Error authenticating!', error);
    });

  }

  logout() {
    this.f.logout();
    this.authenticated=false;
    console.log("logout called");
  }


}
