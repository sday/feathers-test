define('app',['exports', 'socket.io-client', 'feathers-client'], function (exports, _socket, _feathersClient) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.App = undefined;

  var _socket2 = _interopRequireDefault(_socket);

  var _feathersClient2 = _interopRequireDefault(_feathersClient);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var App = exports.App = function () {
    function App() {
      _classCallCheck(this, App);

      this.email = "";
      this.authenticated = "false";

      var parent = this;
      var socket = (0, _socket2.default)('http://localhost:3030');

      this.f = (0, _feathersClient2.default)().configure(_feathersClient2.default.socketio(socket)).configure(_feathersClient2.default.hooks()).configure(_feathersClient2.default.authentication({ storage: window.localStorage }));

      this.f.authenticate().then(function (result) {
        console.log('Already Authenticated!', parent.f.get('token'));
        parent.email = parent.f.get('user').email;
        console.log("User:", parent.f.get('user'));
        parent.authenticated = true;
      }).catch(function (error) {
        console.error('Not authenticated!', error);
        parent.authenticated = false;
      });
    }

    App.prototype.validateEmail = function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    };

    App.prototype.createUser = function createUser() {
      var parent = this;

      if (!this.validateEmail(this.email)) {
        console.log("INVALID EMAIL, UNABLE TO CREATE USER");
        return;
      }
      this.f.service('users').create({
        email: this.email,
        password: "test"
      }).then(function () {
        console.log("%s user created", parent.email);
      }).catch(function (error) {
        console.error('Error Creating User! %s', parent.email, error);
      });
    };

    App.prototype.login = function login() {
      var parent = this;
      console.log("attempting to login with: ", this.email);

      this.f.authenticate({
        type: 'local',
        email: this.email,
        password: "test"
      }).then(function (result) {
        parent.authenticated = true;
        console.log('Authenticated!', parent.f.get('token'));
        console.log('User=', parent.f.get('user').email);
      }).catch(function (error) {
        parent.authenticated = false;
        console.error('Error authenticating!', error);
      });
    };

    App.prototype.logout = function logout() {
      this.f.logout();
      this.authenticated = false;
      console.log("logout called");
    };

    return App;
  }();
});
define('environment',["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    debug: true,
    testing: true
  };
});
define('main',['exports', './environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.configure = configure;

  var _environment2 = _interopRequireDefault(_environment);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  Promise.config({
    warnings: {
      wForgottenReturn: false
    }
  });

  function configure(aurelia) {
    aurelia.use.standardConfiguration().feature('resources');

    if (_environment2.default.debug) {
      aurelia.use.developmentLogging();
    }

    if (_environment2.default.testing) {
      aurelia.use.plugin('aurelia-testing');
    }

    aurelia.start().then(function () {
      return aurelia.setRoot();
    });
  }
});
define('resources/index',["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.configure = configure;
  function configure(config) {}
});
define('text!app.html', ['module'], function(module) { module.exports = "<template>\n  <h1>Feathers Test</h1>\n  <div>app.authenticated=${authenticated}</div>\n  <li>\n    <input value.bind=\"email\" type=\"text\" value=\"\" placeholder=\"Email address\"/>\n    <button click.delegate=\"createUser()\">Create User</button> creates with password= test\n  </li>\n  <li>\n    <button click.delegate=\"login()\">login</button>\n  </li>\n  <li>\n    <button click.delegate=\"logout()\">logout</button>\n  </li>\n</template>\n"; });
//# sourceMappingURL=app-bundle.js.map